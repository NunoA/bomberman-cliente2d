

#include <windows.h>
#include <stdlib.h>
#include <string.h>
#include <tchar.h>
#include <io.h>
#include <conio.h>
#include <fcntl.h>
#include <stdio.h>
#include <aclapi.h>
#include <strsafe.h>

#include "Table.h"
#include <iostream>
using namespace std;
#include "DLL.h"// Adicionada pelo LINKER o .lib

// Global variables
#define btnConnect      1001
#define btnStart		1002
#define btnHighScores   1003
#define btnStop			1004


HANDLE hPipeComunica;
HANDLE hPipeBroadcast;
HANDLE hThreadEscreve, hThreadLer, hThreadDifunde;
HANDLE hThreadTimer;
HANDLE hThreadBomba;

#define PIPE_COM TEXT("\\\\192.168.1.3\\pipe\\comunica")
#define PIPE_BROAD TEXT("\\\\192.168.1.3\\pipe\\broadcast")

DWORD WINAPI lerPipe(LPVOID param);
//DWORD WINAPI escreverPipe(char* accao);
DWORD WINAPI lerPipeDifunde(LPVOID param);
DWORD WINAPI iniciarComunicacao();
DWORD WINAPI carregaImagens(HDC hdc);
DWORD WINAPI timerJogo();
DWORD WINAPI timerBomba();
int test = 0;

//TCHAR bufa[256];
BOOL ret;
Imagens imagens;
int muta = 0;
Table tabuleiro;
Mensagem mensagem;
int estado = 0, background = 0, hscores = 0 ;
int minutos = 0, segundos = 0;
HBITMAP fundo, fundo2;
HighScores hiscores;
HWND hWnd;
Bombas bombas;
// The main window class name.
static TCHAR szWindowClass[] = _T("win32app");

// The string that appears in the application's title bar.
static TCHAR szTitle[] = _T("BomberMan 2D by Nuno Alves & F�bio Costa");

HINSTANCE hInst;

HWND btnConectar, btnIniciar, btnPontuacoes,btnSair;

// Forward declarations of functions included in this code module:
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	int nCmdShow)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_APPLICATION));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_APPLICATION));

	if (!RegisterClassEx(&wcex))
	{
		MessageBox(NULL,
			_T("Call to RegisterClassEx failed!"),
			_T("BomberMan 2D"),
			NULL);

		return 1;
	}

	hInst = hInstance; 
	
	 hWnd = CreateWindow(
		szWindowClass,
		szTitle,
		WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU/* | WS_MINIMIZEBOX */,
		CW_USEDEFAULT, CW_USEDEFAULT,
		1214, 716,
		NULL,
		NULL,
		hInstance,
		NULL
		);

	if (!hWnd)
	{
		MessageBox(NULL,
			_T("Call to CreateWindow failed!"),
			_T("BomberMan 2D "),
			NULL);

		return 1;
	}

	// The parameters to ShowWindow explained:
	// hWnd: the value returned from CreateWindow
	// nCmdShow: the fourth parameter from WinMain
	ShowWindow(hWnd,nCmdShow);
	UpdateWindow(hWnd);

	// Main message loop:
	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return (int)msg.wParam;
}

/*
* Funcao botao conectar
*/

WNDPROC  btnConectProc;
LRESULT CALLBACK funcbtnConnectar(HWND hwnd, UINT msg, WPARAM wp, LPARAM lp)
{
	switch (msg) {
	case WM_LBUTTONDOWN:

		break;
	case WM_LBUTTONUP:
		iniciarComunicacao();
		ShowWindow(btnConectar, SW_HIDE);
		ShowWindow(btnIniciar, SW_SHOW);
		ShowWindow(btnPontuacoes, SW_SHOW);
		break;
	}
	return CallWindowProc(btnConectProc, hwnd, msg, wp, lp);
}

/*
* Funcao botao sair
*/

WNDPROC  btnSairProc;
LRESULT CALLBACK funcbtnSair(HWND hwnd, UINT msg, WPARAM wp, LPARAM lp)
{
	switch (msg) {
	case WM_LBUTTONDOWN:

		break;
	case WM_LBUTTONUP:
		
		exit(1);
		break;
	}
	return CallWindowProc(btnConectProc, hwnd, msg, wp, lp);
}
/*
* FUNCAO botao Iniciar
*/
WNDPROC  btnIniciarProc;
LRESULT CALLBACK funcbtnIniciar(HWND hwnd, UINT msg, WPARAM wp, LPARAM lp)
{
	switch (msg) {
	case WM_LBUTTONDOWN:
		
		break;
	case WM_LBUTTONUP:
		estado = 1;
		escreverPipe("start", hPipeComunica, mensagem);
		ShowWindow(btnConectar, SW_HIDE);
		ShowWindow(btnIniciar, SW_HIDE);
		ShowWindow(btnPontuacoes, SW_HIDE);
		ShowWindow(btnSair, SW_HIDE);
		break;
	}
	return CallWindowProc(btnIniciarProc, hwnd, msg, wp, lp);
}
WNDPROC  btnScoresProc;
LRESULT CALLBACK funcbtnScores(HWND hwnd, UINT msg, WPARAM wp, LPARAM lp)
{
	switch (msg) {
	case WM_LBUTTONDOWN:

		break;
	case WM_LBUTTONUP:
		
		escreverPipe("h", hPipeComunica, mensagem);
		lerPipe(hPipeComunica);
		break;
	}
	return CallWindowProc(btnScoresProc, hwnd, msg, wp, lp);
}


/*
* FUNCAO DA JANELA PRINCIPAL
*/ 
LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	switch (msg)
	{
	case WM_CREATE:
	{
	
		 fundo = (HBITMAP)LoadImage(NULL, L".\\imagens\\bombermanfundoBMP1.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		 fundo2 = (HBITMAP)LoadImage(NULL, L".\\imagens\\fundoJogo2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		 
		 /*Criar BTNS MENU INICIO*/
		 btnConectar = CreateWindow(TEXT("button"), TEXT("Connect"),
			WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON | WS_BORDER,
			300, 200, 200, 50,
			hwnd, (HMENU)btnConnect, NULL, NULL);	 
		 btnConectProc = (WNDPROC)SetWindowLong(btnConectar, GWL_WNDPROC, (LONG)funcbtnConnectar);

		 btnIniciar = CreateWindow(TEXT("button"), TEXT("Start"),
			 WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON | WS_BORDER,
			 300, 275, 200, 50,
			 hwnd, (HMENU)btnStart, NULL, NULL);
		 btnIniciarProc = (WNDPROC)SetWindowLong(btnIniciar, GWL_WNDPROC, (LONG)funcbtnIniciar);

		 btnPontuacoes = CreateWindow(TEXT("button"), TEXT("Scores"),
			 WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON | WS_BORDER,
			 300, 350, 200, 50,
			 hwnd, (HMENU)btnHighScores, NULL, NULL);

		 btnScoresProc = (WNDPROC)SetWindowLong(btnPontuacoes, GWL_WNDPROC, (LONG)funcbtnScores);
		 btnSair = CreateWindow(TEXT("button"), TEXT("Leave"),
			 WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON | WS_BORDER,
			 300, 425, 200, 50,
			 hwnd, (HMENU)btnStop, NULL, NULL);
		 btnSairProc = (WNDPROC)SetWindowLong(btnSair, GWL_WNDPROC, (LONG)funcbtnSair);


		 ShowWindow(btnIniciar, SW_HIDE);
		 ShowWindow(btnPontuacoes, SW_HIDE);//escoder btn
		break;
	}
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);
		
		if (estado == 0){
			carregaImagens(hdc);
			HDC hMemDC = CreateCompatibleDC(hdc);
			::SelectObject(hMemDC, fundo);
			BitBlt(hdc, 0, 0, 1214, 716, hMemDC, 0, 0, SRCCOPY);
			::DeleteDC(hMemDC);
		}
		else{// estamos a jogar		
			if (background == 0){
				background++;
				hThreadTimer = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)timerJogo, NULL, 0, NULL);
			}
		
			tableToString(tabuleiro, hdc, imagens,minutos,segundos, bombas);
			if (bombas.bomba == TRUE){// tirar bomba
				bombas.bomba = FALSE;
			}
		}
		EndPaint(hWnd, &ps);

		break;
	}
	case WM_ERASEBKGND://a testar
		return 1;
	case WM_KEYUP:
	{
		switch (wParam)
		{
		case VK_LEFT:
			escreverPipe("a",hPipeComunica,mensagem);
			break;
		case VK_RIGHT:
			escreverPipe("d", hPipeComunica, mensagem);
			break;
		case VK_UP:
			escreverPipe("w", hPipeComunica, mensagem);
			break;
		case VK_DOWN:
			escreverPipe("s", hPipeComunica, mensagem);
			break;
		case 0x42:
			escreverPipe("b", hPipeComunica, mensagem);
	//		test = 1;
			break;
		
		default:
			break;
		}

		break;
	}
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		return 0;
	}

	}


	return DefWindowProc(hwnd, msg, wParam, lParam);
}

/*
* funcao fazer comunicacoes com o SERVIDOR
*/

DWORD WINAPI iniciarComunicacao(){

	if (!WaitNamedPipe(PIPE_COM, NMPWAIT_WAIT_FOREVER)) {
		_tprintf(TEXT("[ERRO] Ligar ao pipe '%s'... (WaitNamedPipe)\n"), PIPE_COM);
		exit(-1);
	}

	_tprintf(TEXT("[LEITOR] Liga��o ao escritor... (CreateFile)\n"));
	hPipeComunica = CreateFile(PIPE_COM, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hPipeComunica == NULL) {
		_tprintf(TEXT("[ERRO] Ligar ao pipe '%s'... (CreateFile)\n"), PIPE_COM);
		exit(-1);
	}
	_tprintf(TEXT("[LEITOR]Liguei-me pipe Normal...\n"));

	//******************Ler difunde**********************************
	Sleep(1000);
	if (!WaitNamedPipe(PIPE_BROAD, NMPWAIT_WAIT_FOREVER)) {
		_tprintf(TEXT("[ERRO] Ligar ao pipe '%s'... (WaitNamedPipe)\n"), PIPE_BROAD);
		exit(-1);
	}

	_tprintf(TEXT("[LEITOR] Liga��o ao escritor... (CreateFile)\n"));
	hPipeBroadcast = CreateFile(PIPE_BROAD, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hPipeBroadcast == NULL) {
		_tprintf(TEXT("[ERRO] Ligar ao pipe '%s'... (CreateFile)\n"), PIPE_BROAD);
		exit(-1);
	}
	_tprintf(TEXT("[Cliente]Liguei-me Broadcast...\n"));
	hThreadDifunde = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)lerPipeDifunde, NULL, 0, NULL);

	return 0;
}
/*
* Ler PIPE Broadcast
*@param Handle pipe
*/
DWORD WINAPI lerPipeDifunde(LPVOID param){
	DWORD n;
	int i;
	TCHAR buf[256];

	do {
		ret = ReadFile(hPipeBroadcast, &tabuleiro, sizeof(Table), &n, NULL);

		if (estado == 0){// caso n�o sejamos quem deu start
			ShowWindow(btnConectar, SW_HIDE);
			ShowWindow(btnIniciar, SW_HIDE);
			ShowWindow(btnPontuacoes, SW_HIDE);
			ShowWindow(btnSair, SW_HIDE);
		}
		
		// Verificar se existe bomba
		if (tabuleiro.bomb == TRUE)
		{
			bombas.bomba = FALSE;
			bombas.raio = tabuleiro.players[tabuleiro.playerNumber].bombPower;
			bombas.x = tabuleiro.players[tabuleiro.playerNumber].x;
			bombas.y = tabuleiro.players[tabuleiro.playerNumber].y;
			hThreadBomba = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)timerBomba, NULL, 0, NULL);
			
			
		}

		estado = 1;
		InvalidateRect(hWnd, 0, FALSE);
	} while (1);

}

/*
* Ler PIPE NORMAL
*@param Handle pipe
*/
DWORD WINAPI lerPipe(LPVOID param){
	DWORD n;
	int i;
	TCHAR buf[256];
	HighScores scores;
	n = sizeof(scores);
	char number[4];
	ret = ReadFile(hPipeComunica, &scores, sizeof(HighScores), &n, NULL);
	string text;

	for (i = 0; i < 10; i++){
		text.append("Player:   ");
			text.append(&scores.players[i][0]);
		

		text.append("    Time: ");
		_itoa_s(scores.score[i], number, 10);
		text.append(number);
		text.append("\n");
	}
	std::wstring stemp = std::wstring(text.begin(), text.end());// converter em LPCWSTR
	LPCWSTR sw = stemp.c_str();
	MessageBox(NULL, sw,
		TEXT("  HIGHSCORES  "), MB_OK);
	return 0;
}


DWORD WINAPI carregaImagens(HDC hdc){
	

	imagens.artefacto = (HBITMAP)LoadImage(NULL, L".\\imagens\\artefacto.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	imagens.blank = (HBITMAP)LoadImage(NULL, L".\\imagens\\blank.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	imagens.bomb = (HBITMAP)LoadImage(NULL, L".\\imagens\\bomb.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	imagens.extralife = (HBITMAP)LoadImage(NULL, L".\\imagens\\extraLife.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	imagens.extrapower = (HBITMAP)LoadImage(NULL, L".\\imagens\\powerBomb.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	imagens.fundoJogo = (HBITMAP)LoadImage(NULL, L".\\imagens\\fundoJogo2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	imagens.iblock = (HBITMAP)LoadImage(NULL, L".\\imagens\\iblock.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	imagens.inimigo = (HBITMAP)LoadImage(NULL, L".\\imagens\\enemy.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	imagens.jogador = (HBITMAP)LoadImage(NULL, L".\\imagens\\bombermanfront.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	imagens.jogador2 = (HBITMAP)LoadImage(NULL, L".\\imagens\\bombermanfront2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	imagens.nblock = (HBITMAP)LoadImage(NULL, L".\\imagens\\softblock.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	imagens.nblockCastanho = (HBITMAP)LoadImage(NULL, L".\\imagens\\softBlockCastanho.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	imagens.nuke = (HBITMAP)LoadImage(NULL, L".\\imagens\\nukebomb.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	imagens.gate = (HBITMAP)LoadImage(NULL, L".\\imagens\\portal.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);

	//Logos mini
	imagens.bombMini = (HBITMAP)LoadImage(NULL, L".\\imagens\\bomb_mini.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	imagens.powerMini = (HBITMAP)LoadImage(NULL, L".\\imagens\\powerBomb_mini.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	imagens.lifeMini = (HBITMAP)LoadImage(NULL, L".\\imagens\\extraLife_mini.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);

	// Bombas anima��o imagens
	imagens.horizontalI = (HBITMAP)LoadImage(NULL, L".\\imagens\\horizontalI.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	imagens.horizontalFdir = (HBITMAP)LoadImage(NULL, L".\\imagens\\horizontalFdir.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	imagens.horizontalFesq = (HBITMAP)LoadImage(NULL, L".\\imagens\\horizontalFEsq.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);

	imagens.verticalI = (HBITMAP)LoadImage(NULL, L".\\imagens\\verticalI.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	imagens.verticalFbot = (HBITMAP)LoadImage(NULL, L".\\imagens\\vericalFBot.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	imagens.verticalFTop = (HBITMAP)LoadImage(NULL, L".\\imagens\\verticalFTop.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);

	HFONT hfont;
	LOGFONT logFont;
	memset(&logFont, 0, sizeof(logFont));
	logFont.lfHeight = -30; // see PS
	logFont.lfWeight = FW_BOLD;


	hfont = CreateFontIndirect(&logFont);
	HFONT oldHFont = (HFONT)SelectObject(hdc, hfont);

	imagens.hfont = hfont;

	bombas.bomba = FALSE;
	bombas.raio = 0;
	bombas.x = 0;
	bombas.y = 0;

	return 0;
}

DWORD WINAPI timerJogo(){
	//Sleep(3500);//asdasd
	while (1){
		Sleep(1000);
		
		segundos++;
		if (segundos > 60)
		{
			minutos++;
			segundos = 0;
			
		}
		InvalidateRect(hWnd, 0, FALSE);
	}
}

// THREAD conta bombas

DWORD WINAPI timerBomba( ){
	//guardo valores
	int x = bombas.x;
	int y = bombas.y;
	int raio = bombas.raio;
	
	Sleep(2600);
	
	// mete bomba com valores guardados
	
	bombas.bomba = TRUE;
	bombas.raio = raio;
	bombas.x = x;
	bombas.y = y;
	InvalidateRect(hWnd, 0, FALSE);
	return 0;
}