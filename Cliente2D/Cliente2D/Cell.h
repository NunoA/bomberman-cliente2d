
/*
-blocos indestrutiveis
-blocos com vida
-
*/

typedef enum{
	BLANK,
	PLAYER,
	ENEMY,
	BLOCK,
	INDESTRUCTIBLE_BLOCK,
	ARTFACT,
	EXTRALIFE,
	EXTRAPOWER,
	NUKEBOMB,
	BOMB,
	GATE
}ObjectTypes;

typedef struct {
	int x;
	int y;
	int health;		// the health belong to the 1st object in types
	ObjectTypes types[2];
	int playerNumber;
}Cell;


typedef struct{
	int x;
	int y;
	int health;
	int bombs;
	int bombPower;
	ObjectTypes type;
	int number;
}Player;