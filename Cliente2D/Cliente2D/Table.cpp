/*
*this class will handle with the table itself
*/
#include "Table.h"




void tableToString(Table t, HDC hdc, Imagens img, int minutos, int segundos,Bombas bombas){// se o jogo comecou flag e print
	TCHAR buff[10];
	HANDLE tDesenhaBomba;
	//system("cls");
	HDC hMemDC = CreateCompatibleDC(hdc);
	
	if (segundos == 1 && minutos < 1){
		::SelectObject(hMemDC, img.fundoJogo);
		BitBlt(hdc, 0, 0, 1214, 716, hMemDC, 0, 0, SRCCOPY);

	}
	//_tprintf(TEXT("\n\n"));
	for (int y = 0; y < TABLE_SIZE; y++){
		_tprintf(TEXT("\t\t"));
		for (int x = 0; x < TABLE_SIZE; x++){
			switch (t.table[x][y].types[0]){

			case INDESTRUCTIBLE_BLOCK:
				//	_tprintf(TEXT("##"));
				::SelectObject(hMemDC, img.iblock);
				BitBlt(hdc, ((x + 1) * 40), ((y + 1) * 40), 40, 40, hMemDC, 0, 0, SRCCOPY);//  (y+1*40), (x+1*40) sltar de 40 em 40 e imprimir			
				break;
			case BLOCK:
				if (t.table[x][y].health == 1)
					::SelectObject(hMemDC, img.nblockCastanho);
				else
					::SelectObject(hMemDC, img.nblock);
				BitBlt(hdc, ((x + 1) * 40), ((y + 1) * 40), 40, 40, hMemDC, 0, 0, SRCCOPY);//  (y+1*40), (x+1*40) sltar de 40 em 40 e imprimir
				//	_tprintf(TEXT("$$"));
				break;
			case PLAYER:
				if (t.table[x][y].playerNumber == 1)
					::SelectObject(hMemDC, img.jogador);
				else
					::SelectObject(hMemDC, img.jogador2);

				BitBlt(hdc, ((x + 1) * 40), ((y + 1) * 40), 40, 40, hMemDC, 0, 0, SRCCOPY);//  (y+1*40), (x+1*40) sltar de 40 em 40 e imprimir
				//_tprintf(TEXT("P%d"), t.table[x][y].playerNumber);
				break;
			case ENEMY:
				::SelectObject(hMemDC, img.inimigo);
				BitBlt(hdc, ((x + 1) * 40), ((y + 1) * 40), 40, 40, hMemDC, 0, 0, SRCCOPY);//  (y+1*40), (x+1*40) sltar de 40 em 40 e imprimir
				//_tprintf(TEXT("E%d"), t.table[x][y].playerNumber);
				break;
			case BLANK:
				::SelectObject(hMemDC, img.blank);
				BitBlt(hdc, ((x + 1) * 40), ((y + 1) * 40), 40, 40, hMemDC, 0, 0, SRCCOPY);//  (y+1*40), (x+1*40) sltar de 40 em 40 e imprimir
				switch (t.table[x][y].types[1]){
				case BOMB:
					::SelectObject(hMemDC, img.bomb);
					BitBlt(hdc, ((x + 1) * 40), ((y + 1) * 40), 40, 40, hMemDC, 0, 0, SRCCOPY);//  (y+1*40), (x+1*40) sltar de 40 em 40 e imprimir

					break;
				case EXTRALIFE:
					::SelectObject(hMemDC, img.extralife);
					BitBlt(hdc, ((x + 1) * 40), ((y + 1) * 40), 40, 40, hMemDC, 0, 0, SRCCOPY);//  (y+1*40), (x+1*40) sltar de 40 em 40 e imprimir
					//	_tprintf(TEXT("EL"));
					break;
				case GATE:
					::SelectObject(hMemDC, img.gate);
					BitBlt(hdc, ((x + 1) * 40), ((y + 1) * 40), 40, 40, hMemDC, 0, 0, SRCCOPY);//  (y+1*40), (x+1*40) sltar de 40 em 40 e imprimir
					//	_tprintf(TEXT("EL"));
					break;
				case ARTFACT:
					::SelectObject(hMemDC, img.artefacto);
					BitBlt(hdc, ((x + 1) * 40), ((y + 1) * 40), 40, 40, hMemDC, 0, 0, SRCCOPY);//  (y+1*40), (x+1*40) sltar de 40 em 40 e imprimir
					//	_tprintf(TEXT("AF"));
					break;
				case EXTRAPOWER:
					::SelectObject(hMemDC, img.extrapower);
					BitBlt(hdc, ((x + 1) * 40), ((y + 1) * 40), 40, 40, hMemDC, 0, 0, SRCCOPY);//  (y+1*40), (x+1*40) sltar de 40 em 40 e imprimir
					//	_tprintf(TEXT("EP"));
					break;
				case NUKEBOMB:
					::SelectObject(hMemDC, img.nuke);
					BitBlt(hdc, ((x + 1) * 40), ((y + 1) * 40), 40, 40, hMemDC, 0, 0, SRCCOPY);//  (y+1*40), (x+1*40) sltar de 40 em 40 e imprimir
					//	_tprintf(TEXT("NB"));
					break;
				default:
					//_tprintf(TEXT("__"));
					break;
				}

				break;
			default:
				break;
			}
		}
		_tprintf(TEXT("\n"));
	}
	_tprintf(TEXT("\n\n\n"));

	HFONT oldHFont = (HFONT)SelectObject(hdc, img.hfont);

	// converter tempo**************************
	char segundosT[2];

	if (segundos < 10){
		segundosT[0] = '0';
		
		TextOut(hdc, 900, 5, buff, wsprintf(buff, TEXT("%d : %c%d"), minutos, segundosT[0], segundos));
	}
	else{
		TextOut(hdc, 900, 5, buff, wsprintf(buff, TEXT("%d : %d"), minutos, segundos));
	}
	
	if (bombas.bomba == TRUE){
		DesenhaBombas des;
		des.x = bombas.x;
		des.y = bombas.y;
		des.raio = bombas.raio;
		des.hdc = hdc;
		des.t = t;
		des.img = img;

		tDesenhaBomba = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)desenhaExplodeBomba, (LPVOID)&des, 0, NULL);
		Sleep(550);
	//	desenhaExplodeBomba(bombas.x, bombas.y, bombas.raio, t, hdc, img);
	}

	imprimeDadosJogadores(t, hdc,img);

	::DeleteDC(hMemDC);
}


/*
* Imprimir dados dos jogadores
*/
void imprimeDadosJogadores(Table t, HDC hdc, Imagens img){

	int jogadores = getNumberofPlayers(t);
	int vida = 0, bombas = 0, power = 0;
	TCHAR player1[] = _T("Player 1  ");
	TCHAR player2[] = _T("Player 2  ");
	TCHAR enemy1[] = _T("Enemy 1  ");
	TCHAR enemy2[] = _T("Enemy 2  ");

	TCHAR buff[5];
	char vidaT[2];
	char portal[1];
	
	TCHAR keys[] = _T("Keys ");

	HDC hMemDC = CreateCompatibleDC(hdc);

	if (t.numberOfArtfacts < 10)
	{
		portal[0] = '0';
		TextOut(hdc, 735, 30, keys, _tcslen(keys));
		TextOut(hdc, 830, 30, buff, wsprintf(buff, TEXT("%c%d"), portal[0], t.numberOfArtfacts));
	}
	else{

		TextOut(hdc, 735, 30, keys, _tcslen(keys));
		TextOut(hdc,830, 30, buff, wsprintf(buff, TEXT("%d"), t.numberOfArtfacts));
	}



	if (jogadores == 3){//1 humano

		::SelectObject(hMemDC, img.lifeMini);
		BitBlt(hdc, 875, 78, 35, 35, hMemDC, 0, 0, SRCCOPY);//  vida img
		::SelectObject(hMemDC, img.bombMini);
		BitBlt(hdc, 970, 75, 35, 35, hMemDC, 0, 0, SRCCOPY);//  vida img
		::SelectObject(hMemDC, img.powerMini);
		BitBlt(hdc, 1060, 75, 35, 35, hMemDC, 0, 0, SRCCOPY);//  vida img
		TextOut(hdc,735, 75,player1, _tcslen(player1));//player 1
		vida = t.players[0].health;
		bombas = t.players[0].bombs;
		power = t.players[0].bombPower;

		//tratar vida <10
		if (vida < 0)
			vida = 0;
		if (vida < 10){
			vidaT[0] = '0';
			TextOut(hdc, 920, 75, buff, wsprintf(buff, TEXT("%c%d"), vidaT[0], vida));
		}
		else{
			TextOut(hdc, 920, 75, buff, wsprintf(buff, TEXT("%d"), vida));
		}
		//TextOut(hdc, 920, 75, buff, wsprintf(buff, TEXT("%d"), vida));

		TextOut(hdc, 1010, 75, buff, wsprintf(buff, TEXT("%d"), bombas));
		TextOut(hdc, 1100,75, buff, wsprintf(buff, TEXT("%d"), power));
		
		// Player 1 FIM

		//Enemy 1
		TextOut(hdc, 735, 125, enemy1, _tcslen(enemy1));
		::SelectObject(hMemDC, img.lifeMini);
		BitBlt(hdc, 875, 128, 35, 35, hMemDC, 0, 0, SRCCOPY);//  vida img
		::SelectObject(hMemDC, img.bombMini);
		BitBlt(hdc, 970, 125, 35, 35, hMemDC, 0, 0, SRCCOPY);//  vida img
		::SelectObject(hMemDC, img.powerMini);
		BitBlt(hdc, 1060, 125, 35, 35, hMemDC, 0, 0, SRCCOPY);//  vida img

		vida = t.players[1].health;
		bombas = t.players[1].bombs;
		power = t.players[1].bombPower;
		if (vida < 0)
			vida = 0;

		if (vida < 10){
			vidaT[0] = '0';
			TextOut(hdc, 920, 125, buff, wsprintf(buff, TEXT("%c%d"), vidaT[0], vida));
		}
		else{
			TextOut(hdc, 920, 125, buff, wsprintf(buff, TEXT("%d"), vida));
		}
		//TextOut(hdc, 920, 125, buff, wsprintf(buff, TEXT("%d"), vida));
		TextOut(hdc, 1010, 125, buff, wsprintf(buff, TEXT("%d"), bombas));
		TextOut(hdc, 1100, 125, buff, wsprintf(buff, TEXT("%d"), power));
		//FIM ENEMY 1

		// ENEMY 2
		TextOut(hdc, 735, 175, enemy2, _tcslen(enemy2));
		::SelectObject(hMemDC, img.lifeMini);
		BitBlt(hdc, 875, 178, 35, 35, hMemDC, 0, 0, SRCCOPY);//  vida img
		::SelectObject(hMemDC, img.bombMini);
		BitBlt(hdc, 970, 175, 35, 35, hMemDC, 0, 0, SRCCOPY);//  vida img
		::SelectObject(hMemDC, img.powerMini);
		BitBlt(hdc, 1060, 175, 35, 35, hMemDC, 0, 0, SRCCOPY);//  vida img

		vida = t.players[2].health;
		bombas = t.players[2].bombs;
		power = t.players[2].bombPower;
		if (vida < 0)
			vida = 0;
		if (vida < 10){
			vidaT[0] = '0';
			TextOut(hdc, 920, 175, buff, wsprintf(buff, TEXT("%c%d"), vidaT[0], vida));
		}
		else{
			TextOut(hdc, 920, 175, buff, wsprintf(buff, TEXT("%d"), vida));
		}
		//TextOut(hdc, 920, 175, buff, wsprintf(buff, TEXT("%d"), vida));
		TextOut(hdc, 1010, 175, buff, wsprintf(buff, TEXT("%d"), bombas));
		TextOut(hdc, 1100, 175, buff, wsprintf(buff, TEXT("%d"), power));
		
	}
	else{//2 humanos
		

		TextOut(hdc, 735, 75, player1, _tcslen(player1));//player 1
		::SelectObject(hMemDC, img.lifeMini);
		BitBlt(hdc, 875, 78, 35, 35, hMemDC, 0, 0, SRCCOPY);//  vida img
		::SelectObject(hMemDC, img.bombMini);
		BitBlt(hdc, 970, 75, 35, 35, hMemDC, 0, 0, SRCCOPY);//  vida img
		::SelectObject(hMemDC, img.powerMini);
		BitBlt(hdc, 1060, 75, 35, 35, hMemDC, 0, 0, SRCCOPY);//  vida img

		vida = t.players[0].health;
		bombas = t.players[0].bombs;
		power = t.players[0].bombPower;
		if (vida < 0)
			vida = 0;
		if (vida < 10){
			vidaT[0] = '0';
			TextOut(hdc, 920, 75, buff, wsprintf(buff, TEXT("%c%d"), vidaT[0], vida));
		}
		else{
			TextOut(hdc, 920, 75, buff, wsprintf(buff, TEXT("%d"), vida));
		}
		//TextOut(hdc, 920, 75, buff, wsprintf(buff, TEXT("%d"), vida));
		TextOut(hdc, 1010, 75, buff, wsprintf(buff, TEXT("%d"), bombas));
		TextOut(hdc, 1100, 75, buff, wsprintf(buff, TEXT("%d"), power));

		// Player 1 FIM

		//PLAYER 2
		TextOut(hdc, 735, 125, player2, _tcslen(player2));//player 2
		::SelectObject(hMemDC, img.lifeMini);
		BitBlt(hdc, 875, 128, 35, 35, hMemDC, 0, 0, SRCCOPY);//  vida img
		::SelectObject(hMemDC, img.bombMini);
		BitBlt(hdc, 970, 125, 35, 35, hMemDC, 0, 0, SRCCOPY);//  vida img
		::SelectObject(hMemDC, img.powerMini);
		BitBlt(hdc, 1060, 125, 35, 35, hMemDC, 0, 0, SRCCOPY);//  vida img

		vida = t.players[1].health;
		bombas = t.players[1].bombs;
		power = t.players[1].bombPower;
		if (vida < 0)
			vida = 0;
		if (vida < 10){
			vidaT[0] = '0';
			TextOut(hdc, 920, 128, buff, wsprintf(buff, TEXT("%c%d"), vidaT[0], vida));
		}
		else{
			TextOut(hdc, 920, 128, buff, wsprintf(buff, TEXT("%d"), vida));
		}
		//TextOut(hdc, 925, 128, buff, wsprintf(buff, TEXT("%d"), vida));
		TextOut(hdc, 1010, 125, buff, wsprintf(buff, TEXT("%d"), bombas));
		TextOut(hdc, 1100, 125, buff, wsprintf(buff, TEXT("%d"), power));

		//FIM PLAYER 2

		//Enemy 1
		TextOut(hdc, 735, 175, enemy1, _tcslen(enemy1));
		::SelectObject(hMemDC, img.lifeMini);
		BitBlt(hdc, 875, 178, 35, 35, hMemDC, 0, 0, SRCCOPY);//  vida img
		::SelectObject(hMemDC, img.bombMini);
		BitBlt(hdc, 970, 175, 35, 35, hMemDC, 0, 0, SRCCOPY);//  vida img
		::SelectObject(hMemDC, img.powerMini);
		BitBlt(hdc, 1060, 175, 35, 35, hMemDC, 0, 0, SRCCOPY);//  vida img

		vida = t.players[2].health;
		bombas = t.players[2].bombs;
		power = t.players[2].bombPower;
		if (vida < 0)
			vida = 0;
		if (vida < 10){
			vidaT[0] = '0';
			TextOut(hdc, 920, 175, buff, wsprintf(buff, TEXT("%c%d"), vidaT[0], vida));
		}
		else{
			TextOut(hdc, 920, 175, buff, wsprintf(buff, TEXT("%d"), vida));
		}
		//TextOut(hdc, 920, 175, buff, wsprintf(buff, TEXT("%d"), vida));
		TextOut(hdc, 1010, 175, buff, wsprintf(buff, TEXT("%d"), bombas));
		TextOut(hdc, 1100, 175, buff, wsprintf(buff, TEXT("%d"), power));
		//FIM ENEMY 1

		// ENEMY 2
		TextOut(hdc, 735, 225, enemy2, _tcslen(enemy2));
		::SelectObject(hMemDC, img.lifeMini);
		BitBlt(hdc, 875, 228, 35, 35, hMemDC, 0, 0, SRCCOPY);//  vida img
		::SelectObject(hMemDC, img.bombMini);
		BitBlt(hdc, 970, 225, 35, 35, hMemDC, 0, 0, SRCCOPY);//  vida img
		::SelectObject(hMemDC, img.powerMini);
		BitBlt(hdc, 1060, 225, 35, 35, hMemDC, 0, 0, SRCCOPY);//  vida img

		vida = t.players[3].health;
		bombas = t.players[3].bombs;
		power = t.players[3].bombPower;
		if (vida < 0)
			vida = 0;
		if (vida < 10){
			vidaT[0] = '0';
			TextOut(hdc, 920, 225, buff, wsprintf(buff, TEXT("%c%d"), vidaT[0], vida));
		}
		else{
			TextOut(hdc, 920, 225, buff, wsprintf(buff, TEXT("%d"), vida));
		}
		//TextOut(hdc, 920, 225, buff, wsprintf(buff, TEXT("%d"), vida));
		TextOut(hdc, 1010, 225, buff, wsprintf(buff, TEXT("%d"), bombas));
		TextOut(hdc, 1100, 225, buff, wsprintf(buff, TEXT("%d"), power));
		//FIM ENEMY 2
		
	}
	::DeleteDC(hMemDC);
}

int getNumberofPlayers(Table t){
	int players = 0;

	for (int i = 0; i < 4; i++){
		if (t.players[i].type != BLANK)
			players++;

	}
	return players;
}
/*
* - Funcao para imprimir a explosao das bombas
*/
void desenhaExplodeBomba(/*int x, int y, int raio, Table t, HDC hdc, Imagens img*/LPVOID *param){

	DesenhaBombas *des = (DesenhaBombas*)param;
	
	int x = des->x;
	int y = des->y;
	int raio = des->raio;
	HDC hdc = des->hdc;
	Imagens img = des->img;
	Table t = des->t;
	HDC hMemDC = CreateCompatibleDC(hdc);

	//4x for para cada direccao
	/*
	* horizontal direita
	*/
	
	for (int i = 1; i < raio + 1; i++){
		//HORIZONTAL
		if (/*t.table[x + i + 1][y + 1].types[0] != INDESTRUCTIBLE_BLOCK &&*/ (x + i ) < 15)//drt
		{
			if (i < raio)
			{
				::SelectObject(hMemDC, img.horizontalI);
				BitBlt(hdc, ((x + i+1) * 40), ((y + 1) * 40), 40, 40, hMemDC, 0, 0, SRCCOPY);
			}
			else{
				::SelectObject(hMemDC, img.horizontalFdir);
				BitBlt(hdc, ((x + i+1) * 40), ((y + 1) * 40), 40, 40, hMemDC, 0, 0, SRCCOPY);
			}

		}
		if (/*t.table[x - i + 1][y + 1].types[0] != INDESTRUCTIBLE_BLOCK &&*/ (x - i + 1) > 0)//esq
		{
			if (i < raio)
			{
				::SelectObject(hMemDC, img.horizontalI);
				BitBlt(hdc, ((x - i+1) * 40), ((y + 1) * 40), 40, 40, hMemDC, 0, 0, SRCCOPY);
			}
			else{
				::SelectObject(hMemDC, img.horizontalFesq);
				BitBlt(hdc, ((x - i+1) * 40), ((y + 1) * 40), 40, 40, hMemDC, 0, 0, SRCCOPY);
			}

		}
		//VERTICAL
		if (/*t.table[x + 1][y + i + 1].types[0] != INDESTRUCTIBLE_BLOCK && */(y + i) < 15)//cima
		{
			if (i < raio)
			{
				::SelectObject(hMemDC, img.verticalI);
				BitBlt(hdc, ((x + 1) * 40), ((y + i+1) * 40), 40, 40, hMemDC, 0, 0, SRCCOPY);
			}
			else{
				::SelectObject(hMemDC, img.verticalFbot);
				BitBlt(hdc, ((x + 1) * 40), ((y + i+1) * 40), 40, 40, hMemDC, 0, 0, SRCCOPY);
			}

		}
		if (/*t.table[x + 1][y - i + 1].types[0] != INDESTRUCTIBLE_BLOCK && */(y - i+1) > 0)//baixo
		{
			if (i < raio)
			{
				::SelectObject(hMemDC, img.verticalI);
				BitBlt(hdc, ((x + 1) * 40), ((y - i+1) * 40), 40, 40, hMemDC, 0, 0, SRCCOPY);
			}
			else{
				::SelectObject(hMemDC, img.verticalFTop);
				BitBlt(hdc, ((x + 1) * 40), ((y - i+1) * 40), 40, 40, hMemDC, 0, 0, SRCCOPY);
			}

		}

		Sleep(150);
	}
	::DeleteDC(hMemDC);

}

