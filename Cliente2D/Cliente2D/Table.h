#include "Cell.h"

#include <time.h>
#include <stdlib.h>
#include <tchar.h>
#include <windows.h>
#include <stdio.h>


#define TABLE_SIZE 15
#define TAM 15
#define NUMBER_OF_INDESTRUCTIBLE_BLOCKS 20
#define NUMBER_OF_BLOCKS 50
#define NUMBBER_OF_PLAYERS 4



typedef struct {
	Cell table[TAM][TAM];
	Player players[NUMBBER_OF_PLAYERS];
	BOOLEAN bomb;
	int playerNumber;
	int numberOfArtfacts;
}Table;

typedef struct{
	HBITMAP fundoJogo;
	HBITMAP jogador;
	HBITMAP jogador2;
	HBITMAP inimigo;
	HBITMAP iblock;
	HBITMAP nblock;
	HBITMAP nblockCastanho;
	HBITMAP blank;
	HBITMAP bomb;
	HBITMAP extralife;
	HBITMAP artefacto;
	HBITMAP extrapower;
	HBITMAP nuke;
	HBITMAP gate;
	HBITMAP bombMini;
	HBITMAP powerMini;
	HBITMAP lifeMini;

	HBITMAP horizontalI;
	HBITMAP horizontalFesq;
	HBITMAP horizontalFdir;
	HBITMAP verticalI;
	HBITMAP verticalFTop;
	HBITMAP verticalFbot;

	
	HFONT hfont;
}Imagens;

typedef struct{
	int x;
	int y;
	int raio;
	BOOL bomba;
}Bombas;

typedef struct{
	HDC hdc;
	Table t;
	int x;
	int y;
	int raio;
	Imagens img;
}DesenhaBombas;




void tableToString(Table t, HDC hdc, Imagens img, int minutos, int segundos,Bombas bombas);
//criar fonte
void imprimeDadosJogadores(Table t, HDC hdc, Imagens img);
int getNumberofPlayers(Table t);
void desenhaExplodeBomba(/*int x, int y, int raio, Table t, HDC hdc, Imagens imgLPVOID */LPVOID *param);